package pe.uni.rsaenzc.mitrivia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TriviaActivity extends AppCompatActivity {

    Button button_verdadero,button_falso,button_anterior,button_siguiente;
    TextView textViewPregunta;
    ImageView image_view;
    int opcion=0;
    int alternativa=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trivia);

        button_anterior=findViewById(R.id.button_anterior);
        button_siguiente=findViewById(R.id.button_siguiente);
        button_falso=findViewById(R.id.button_falso);
        button_verdadero=findViewById(R.id.button_verdadero);
        textViewPregunta=findViewById(R.id.pregunta);
        image_view=findViewById(R.id.image_view);


        button_verdadero.setOnClickListener(v -> {
            if(alternativa==0){
                Toast.makeText(getApplicationContext(), "Incorecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==1){
                Toast.makeText(getApplicationContext(), "Incorecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==2){
                Toast.makeText(getApplicationContext(), "Corecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==3){
                Toast.makeText(getApplicationContext(), "Corecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==4){
                Toast.makeText(getApplicationContext(), "Incorecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==5){
                Toast.makeText(getApplicationContext(), "Incorecto", Toast.LENGTH_LONG).show();
            }
        });

        button_falso.setOnClickListener(v -> {
            if(alternativa==0){
                Toast.makeText(getApplicationContext(), "Corecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==1){
                Toast.makeText(getApplicationContext(), "Corecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==2){
                Toast.makeText(getApplicationContext(), "Incorecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==3){
                Toast.makeText(getApplicationContext(), "Incorecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==4){
                Toast.makeText(getApplicationContext(), "Corecto", Toast.LENGTH_LONG).show();
            }
            if(alternativa==5){
                Toast.makeText(getApplicationContext(), "Corecto", Toast.LENGTH_LONG).show();
            }
        });


        button_siguiente.setOnClickListener(v -> {
            opcion=opcion+1;
            if(opcion==1){
                textViewPregunta.setText(R.string.pregunta2);
                image_view.setImageResource(R.drawable.b);
                alternativa=1;
            }
            if(opcion==2){
                textViewPregunta.setText(R.string.pregunta3);
                image_view.setImageResource(R.drawable.c);
                alternativa=2;
            }
            if(opcion==3){
                textViewPregunta.setText(R.string.pregunta4);
                image_view.setImageResource(R.drawable.d);
                alternativa=3;
            }
            if(opcion==4){
                textViewPregunta.setText(R.string.pregunta5);
                image_view.setImageResource(R.drawable.e);
                alternativa=4;
            }
            if(opcion==5){
                textViewPregunta.setText(R.string.pregunta6);
                image_view.setImageResource(R.drawable.f);
                alternativa=5;
            }
            if(opcion==6){
                opcion=0;
                textViewPregunta.setText(R.string.pregunta1);
                image_view.setImageResource(R.drawable.a);
                alternativa=0;
            }
        });

        button_anterior.setOnClickListener(v -> {
            opcion=opcion-1;
            if(opcion==1){
                textViewPregunta.setText(R.string.pregunta2);
                image_view.setImageResource(R.drawable.b);
                alternativa=1;
            }
            if(opcion==2){
                textViewPregunta.setText(R.string.pregunta3);
                image_view.setImageResource(R.drawable.c);
                alternativa=2;
            }
            if(opcion==3){
                textViewPregunta.setText(R.string.pregunta4);
                image_view.setImageResource(R.drawable.d);
                alternativa=3;
            }
            if(opcion==4){
                textViewPregunta.setText(R.string.pregunta5);
                image_view.setImageResource(R.drawable.e);
                alternativa=4;
            }
            if(opcion==5){
                textViewPregunta.setText(R.string.pregunta6);
                image_view.setImageResource(R.drawable.f);
                alternativa=5;
            }
            if(opcion<1 || opcion>5){
                opcion=0;
                textViewPregunta.setText(R.string.pregunta1);
                image_view.setImageResource(R.drawable.a);
                alternativa=0;
            }
        });


    }
}